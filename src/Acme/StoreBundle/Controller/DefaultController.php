<?php

namespace Acme\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\StoreBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeStoreBundle:Default:index.html.twig');
    }
    
    public function createAction($NOMBRE){
        $product = new Product();
        $product->setName($NOMBRE);
        $product->setPrice('19.99');
        $product->setDescription('Lorem ipsum dolor');
 
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();
 
        return new Response('Created product id '.$product->getId());
    }
    
    public function showAction($id){
        $product = $this->getDoctrine()
            ->getRepository('AcmeStoreBundle:Product')
            ->find($id);
 
        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        
        return $this->render('AcmeStoreBundle:Default:index.html.twig', array('name' => $product->getName(), 'precio' => $product->getPrice(), 'descripcion' => $product->getDescription()));
        // ... (pasar el objeto $product a una plantilla)
    }

}


