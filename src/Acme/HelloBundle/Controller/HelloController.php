<?php
// src/Acme/HelloBundle/Controller/HelloController.phpreturn $this->render('AcmeHelloBundle:Hello:index.html.twig', array('name' => $name));

namespace Acme\HelloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HelloController extends Controller{
	public function indexAction($name){
		return $this->render('AcmeHelloBundle:Hello:index.html.twig', array('name' => $name));
	}
}

?>
